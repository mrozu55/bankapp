package com.bank.CreateAccount;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

public class CreateAccountView {
    final private Pane parent;
    private CreateAccountPresenter presenter;
    private TextField nick;
    private TextField name;
    private TextField last_name;
    private PasswordField password;
    private PasswordField password_repeat;
    private TextField email;
    private TextField phone;
    private GridPane gridPane;

    public CreateAccountView(Pane parent) {
        this.parent = parent;
    }

    public void setPresenter(CreateAccountPresenter presenter) {
        this.presenter = presenter;
    }

    public void showCreator() {
        //Poprawić wygląd okienek
        nick = new TextField();
        name = new TextField();
        last_name = new TextField();
        password = new PasswordField();
        password_repeat = new PasswordField();
        email = new TextField();
        phone = new TextField();

        Button register_button = new Button("Rejestracja");
        Button cancel_button = new Button("Anuluj");

        register_button.setOnAction(event -> presenter.createAccount());
        cancel_button.setOnAction(event -> presenter.backToLogin());
        gridPane = new GridPane();
        //gridPane.setMinSize(200, 600);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(5);
        gridPane.setHgap(5);
        gridPane.setAlignment(Pos.TOP_CENTER);
        gridPane.addRow(0, new Label("Nick:"), nick);
        gridPane.addRow(1, new Label("Imie:"), name);
        gridPane.addRow(2, new Label("Nazwisko:"), last_name);
        gridPane.addRow(3, new Label("Hasło:"), password);
        gridPane.addRow(4, new Label("Powtórz hasło:"), password_repeat);
        gridPane.addRow(5, new Label("Email:"), email);
        gridPane.addRow(6, new Label("Telefon:"), phone);
        gridPane.addRow(7, register_button, cancel_button);


        parent.getChildren().clear();
        parent.getChildren().add(gridPane);

    }
    protected void addError(String error, int row){
        gridPane.addRow(row, new Label(error));
    }

    protected TextField getNick() {
        return nick;
    }

    protected TextField getName() {
        return name;
    }

    protected TextField getLast_name() {
        return last_name;
    }

    protected PasswordField getPassword() {
        return password;
    }

    protected PasswordField getPassword_repeat() {
        return password_repeat;
    }

    protected TextField getEmail() {
        return email;
    }

    protected TextField getPhone() {
        return phone;
    }

    protected Pane getParent() {
        return parent;
    }
}
