package com.bank.CreateAccount;

import javafx.scene.layout.Pane;

public class CreateAccountPresenterFactory {
    private final Pane parent;
    public CreateAccountPresenterFactory(Pane parent) {
        this.parent = parent;
    }

    public CreateAccountPresenter create(){
        CreateAccountView view = new CreateAccountView(parent);
        CreateAccountPresenter presenter = new CreateAccountPresenter(view);
        view.setPresenter(presenter);
        return presenter;
    }


}
