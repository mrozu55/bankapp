package com.bank.CreateAccount;

import com.bank.DAO.ClientDAO;
import com.bank.DAO.ClientDAO_DB;
import com.bank.InformationWindow.BoxWindow;
import com.bank.Login.LoginWindowPresenterFactory;
import javafx.scene.control.TextField;

public class CreateAccountPresenter {
    final private CreateAccountView view;
    public CreateAccountPresenter(CreateAccountView view) {
        this.view = view;
    }

    public void showCreator(){
        view.showCreator();
    }

    protected void createAccount() {
        setDefaultFieldStyle();
        boolean fields_are_good = validateFields();

        if(fields_are_good){
            if(!createNewAccount()){
                BoxWindow.infoBox("Coś poszło nie tak", "Błąd");
                return;
            }
            backToLogin();
            BoxWindow.infoBox("Dziekujem za założenie nowego konta. W naszym banku troszczymy się o klientów, dlatego dajemu Tobie 1000 zł na start. Możesz już się zalogować na swoje konto", "Witamy");
        }
    }

    private boolean validateFields(){
        boolean is_good = true;
        if(!view.getNick().getText().matches("^[a-zA-Z0-9_]{3,20}+$")){
            setInvalidField(view.getNick());
            is_good =  false;
        }

        if(!view.getName().getText().matches("^[a-zA-Z\\p{L}]{3,20}+$")) {
            setInvalidField(view.getName());
            is_good =  false;
        }

        if(!view.getLast_name().getText().matches("^[a-zA-Z\\p{L}]{3,25}+$")) {
            setInvalidField(view.getLast_name());
            is_good =  false;
        }

        if(!view.getPassword().getText().matches("^(?=.*[0-9])(?=.*[a-zA-Z\\p{L}])(?=\\S+$).{8,}$")) {
            setInvalidField(view.getPassword());
            is_good =  false;
        }
        //repeat password
        if(!view.getPassword_repeat().getText().equals(view.getPassword().getText())) {
            setInvalidField(view.getPassword_repeat());
            is_good =  false;
        }

        if(!view.getEmail().getText().matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$")) {
            setInvalidField(view.getEmail());
            is_good =  false;
        }

        //phone validation
        if(!view.getPhone().getText().matches("^[0-9]{9,12}+$")){
            setInvalidField(view.getPhone());
            is_good =  false;
            //view.addError("Nie poprawny numer telefonu", 6);
        }

        //everything is ok return true
        return is_good;
    }

    private boolean createNewAccount(){
        //Create new unique account number
        NewAccountNumber new_number = new NewAccountNumber();
        new_number.generateNewNumber();

        String account_number = new_number.getAccount_number();
        //Create new client
        NewClient newClient = new NewClient(
                view.getNick().getText(),
                view.getName().getText(),
                view.getLast_name().getText(),
                view.getPassword().getText().toCharArray(),
                view.getEmail().getText(),
                view.getPhone().getText(),
                account_number
        );

        //Add new client to database
        ClientDAO clientDAO= new ClientDAO_DB();
        return clientDAO.addClient(newClient);
    }

    private void setDefaultFieldStyle(){
        view.getNick().getStyleClass().remove("invalidBorder");
        view.getName().getStyleClass().remove("invalidBorder");
        view.getLast_name().getStyleClass().remove("invalidBorder");
        view.getEmail().getStyleClass().remove("invalidBorder");
        view.getPhone().getStyleClass().remove("invalidBorder");
        view.getPassword().getStyleClass().remove("invalidBorder");
        view.getPassword_repeat().getStyleClass().remove("invalidBorder");
    }

    private void setInvalidField(TextField field) {
        field.getStyleClass().add("invalidBorder");
    }


    public void backToLogin() {
        new LoginWindowPresenterFactory(view.getParent())
                .create()
                .showLogin();
    }
}
