package com.bank.CreateAccount;

import com.bank.DAO.ClientDAO_DB;

import java.util.Random;

public class NewAccountNumber {
    private String account_number;
    private final String IBAN = "PL49";

    public void generateNewNumber(){
        //I know it's not the best system for creating account number :D
        //with more clients will be more difficult to find unique number
        //but it's just for fun
        generateNumber();
        while(checkIfExist()){
            generateNumber();
        }
    }

    private void generateNumber(){
        String new_number;
        Random random = new Random();
        int first_part = random.nextInt(9000) + 1000;
        int second_part = random.nextInt(9000) + 1000 ;

        new_number = String.valueOf(first_part) + second_part;
        account_number = IBAN + new_number;

    }

    private boolean checkIfExist(){
        return new ClientDAO_DB().doesAccountNumberExist(account_number);
    }

    public String getAccount_number() {
        return account_number;
    }
}
