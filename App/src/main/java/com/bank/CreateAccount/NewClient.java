package com.bank.CreateAccount;

public class NewClient {
    private final String nick;
    private final String name;
    private final String last_name;
    private final char[] password;
    private final String email;
    private final String phone;
    private final String account_number;

    public NewClient(String nick, String name, String last_name, char[] password, String email, String phone, String account_number) {
        this.nick = nick;
        this.name = name;
        this.last_name = last_name;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.account_number = account_number;
    }

    public String getNick() {
        return nick;
    }

    public String getName() {
        return name;
    }

    public String getLast_name() {
        return last_name;
    }

    public char[] getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getAccount_number() {
        return account_number;
    }
}
