package com.bank.DAO;

import com.bank.AccountDetails.ClientDetails;
import com.bank.CreateAccount.NewClient;
import com.bank.Menu.Client;

import java.math.BigDecimal;

public interface ClientDAO {
    Client getClient(String login, char[] password);
    boolean addClient(NewClient student);
    boolean updateClient(ClientDetails student, char[] password);
    void updateClientPassword(ClientDetails student, char[] old_password, char[] new_password);
    void deleteClient(Client student);
    boolean transferMoney(Client client, char[] password, String ac_number, BigDecimal money);
    boolean doesAccountNumberExist(String number);
}
