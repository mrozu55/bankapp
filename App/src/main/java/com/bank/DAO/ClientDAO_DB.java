package com.bank.DAO;

import com.bank.AccountDetails.ClientDetails;
import com.bank.CreateAccount.NewClient;
import com.bank.InformationWindow.BoxWindow;
import com.bank.Menu.Client;
import java.math.BigDecimal;
import java.sql.*;

public class ClientDAO_DB implements ClientDAO {
    private final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private final String DB_URL = "jdbc:mysql://localhost/bank_db?useUnicode=true&characterEncoding=utf-8";
    private final BigDecimal MONEY_FOR_START = BigDecimal.valueOf(100000); //which is 1000
    private final String USER = "root";
    private final String PASS = "";

    @Override
    public Client getClient(String login, char[] password) {
        Client client;
        Connection conn;
        PreparedStatement pstmt;
        try{
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL,USER,PASS);


            String sql = "Select id, nick, first_name, last_name, email, phone, money, account_number from clients" +
                    " where nick = ? and password = ?";

            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, login);
            pstmt.setString(2, String.valueOf(password));

            ResultSet rs = pstmt.executeQuery(); //Fetch the data from database
            int size= 0;
            if (rs!= null) {
                rs.beforeFirst();
                rs.last();
                size = rs.getRow();
            }
            if(size != 1) {
                throw new Exception("Nie odnaleziono podanego użytkownika");
            }
            else{
                int id = rs.getInt("id");
                String nick = rs.getString("nick");
                String first_name = rs.getString("first_name");
                String last_name = rs.getString("last_name");
                String email = rs.getString("email");
                String phone = rs.getString("phone");
                BigDecimal money = rs.getBigDecimal("money");
                String account_number = rs.getString("account_number");

                client = new Client(
                        id,
                        nick,
                        first_name,
                        last_name,
                        email,
                        phone,
                        money,
                        account_number
                );
                conn.close();
                return client;
            }
        }
        catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean addClient(NewClient newclient) {
        Connection conn;
        PreparedStatement pstmt;
        try{
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

            String sql = "Insert into Clients VALUES(null, ?, ?, ?, ?, ?, ?, ?, ?)";

            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, newclient.getNick());
            pstmt.setString(2, newclient.getName());
            pstmt.setString(3, newclient.getLast_name());
            pstmt.setString(4, String.valueOf(newclient.getPassword()));
            pstmt.setString(5, newclient.getEmail());
            pstmt.setString(6, newclient.getPhone());
            pstmt.setBigDecimal(7, MONEY_FOR_START);
            pstmt.setString(8, newclient.getAccount_number());

            pstmt.executeUpdate();

            return true;
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace(); //comment it later

            return false;
        }
    }

    @Override
    public boolean updateClient(ClientDetails client, char[] password) {
        //For changing account details
        Connection conn;
        Statement stmt;
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL,USER,PASS);


            String sql = "Update Clients set first_name = ?, last_name = ?, email = ?, phone = ?" +
                    " where nick = ? and password = ? ;";

            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, client.getName());
            pstmt.setString(2, client.getLast_name());
            pstmt.setString(3, client.getEmail());
            pstmt.setString(4, client.getPhone());
            pstmt.setString(5, client.getNick());
            pstmt.setString(6, String.valueOf(password));
            int execute = pstmt.executeUpdate();
            if(execute < 1){
                throw new SQLException("Couldn't find an account");
            }
            return true;

        } catch (ClassNotFoundException | SQLException e) {
            //e.printStackTrace(); //Comment it later
            if(e.getMessage().equals("Couldn't find an account")){
                BoxWindow.infoBox("Podano nie poprawne hasło", "Błąd dostępu");
                return false;
            }
            else BoxWindow.infoBox("Coś poszło nie tak", "Błąd");
            return false;
        }
    }

    @Override
    public void updateClientPassword(ClientDetails client, char[] old_password, char[] new_password) {
        //I made separate method for changing password because class Client doesn't have field password
        //so method updateClient() couldn't change password


    }

    @Override
    public void deleteClient(Client student) {
        //later
    }

    @Override
    public boolean transferMoney(Client client, char[] password, String ac_number, BigDecimal money) {
        //transfer money need authorization with password
        Connection conn;
        String transfer_from = "Update Clients set money = money - ? where nick = ? and password = ? ;";
        String transfer_to = "Update Clients set money = money + ? where account_number = ?;";
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

            //update money column in client who sends money
            //Maybe latter try to make database transaction for that

            PreparedStatement pstmt = conn.prepareStatement(transfer_from);
            pstmt.setBigDecimal(1, money);
            pstmt.setString(2, client.getNick());
            pstmt.setString(3, String.valueOf(password));

            int execute = pstmt.executeUpdate();
            if(execute < 1){
                //0 when nothing was changed, -1 when got error
                throw new AuthorizationFailureException();
            }

            PreparedStatement pstmt2 = conn.prepareStatement(transfer_to);
            pstmt2.setBigDecimal(1, money);
            pstmt2.setString(2, ac_number);

            int execute_two = pstmt2.executeUpdate();
            System.out.println(execute_two);
            if(execute_two < 1){
                throw new SQLException("Couldn't found account number");
            }
            conn.close();
            return true;
        } catch (ClassNotFoundException | SQLException | AuthorizationFailureException e) {
            //e.printStackTrace();
            if(e instanceof AuthorizationFailureException){
                BoxWindow.infoBox(e.getMessage(),"Błąd dostępu");
                return false;
            }
            else if(e.getMessage().equals("Couldn't found account number")){
                BoxWindow.infoBox("Podany numer konta jest nie poprawny", "Błąd");
                return false;
            }
            else{
                BoxWindow.infoBox("Coś poszło nie tak", "Błąd");
                return false;
            }


        }
    }

    @Override
    public boolean doesAccountNumberExist(String number) {
        //check if given account number already exist
        Connection conn;
        Statement stmt;
        try{
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            stmt = conn.createStatement();
            String sql = "Select count(*) from clients where account_number = '" + number + "';";
            ResultSet rs = stmt.executeQuery(sql);
            //if sql return result == 0 return true otherwise return false because given account number already exist
            if(rs.next()){
                return rs.getInt(1) != 0;
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            BoxWindow.infoBox("Coś poszło nie tak. Prosze spróbowac jeszcze raz", "Błąd");
        }
        return true;
    }
}

