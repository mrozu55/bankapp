package com.bank;

import com.bank.Menu.Client;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BankHelper {
    public static BigDecimal formatMoney(BigDecimal money){
        BigDecimal rest = new BigDecimal("100");
        return money.divide(rest, 2, RoundingMode.HALF_UP);
    }

    public static String formatAccountNumber(String number){
        return number.substring(0,4) + " " + number.substring(4,8) + " " + number.substring(8);
    }

    public static Client subtractMoney(Client client, BigDecimal money){
        //If given money are greater then client money then return client without changes
        if(client.getMoney().compareTo(money) < 0) return client;
        BigDecimal new_money = client.getMoney().subtract(money);
        client = new Client(
                client.getClientID(),
                client.getNick(),
                client.getName(),
                client.getLast_name(),
                client.getEmail(),
                client.getPhone(),
                new_money,
                client.getAc_number()
        );
        return client;
    }

    public static Client addMoney(Client client, BigDecimal money){
        BigDecimal new_money = client.getMoney().add(money);
        client = new Client(
                client.getClientID(),
                client.getNick(),
                client.getName(),
                client.getLast_name(),
                client.getEmail(),
                client.getPhone(),
                new_money,
                client.getAc_number()
        );
        return client;
    }
}
