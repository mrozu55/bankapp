package com.bank.Login;


import com.bank.CreateAccount.CreateAccountPresenter;
import com.bank.CreateAccount.CreateAccountPresenterFactory;
import com.bank.DAO.ClientDAO_DB;
import com.bank.InformationWindow.BoxWindow;
import com.bank.Menu.Client;
import com.bank.Menu.MenuWindowPresenter;
import com.bank.Menu.MenuWindowPresenterFactory;


public class LoginWindowPresenter {
    private final LoginWindowView view;

    public LoginWindowPresenter(LoginWindowView view) {
        this.view = view;
    }

    public void showLogin() {
        view.showCreator();
    }

    public void login(String login, char[] password) {
        Client client = new ClientDAO_DB().getClient(login, password);
        if(client != null){
            showMenu(client);
        }
        else{
            BoxWindow.infoBox("Nie poprawny login lub hasło", "Błąd logowania");
        }
    }

    public void showMenu(Client client){
        MenuWindowPresenter menuWindowPresenter = new MenuWindowPresenterFactory(view.getParent()).create();
        menuWindowPresenter.setClient(client);
        menuWindowPresenter.showCreator();
    }

    public void createNewAccount() {
        //Need new package
        //Show creator
        CreateAccountPresenter createAccountPresenter = new CreateAccountPresenterFactory(view.getParent()).create();
        createAccountPresenter.showCreator();
    }
}
