package com.bank;

import com.bank.CreateAccount.NewAccountNumber;
import com.bank.InformationWindow.BoxWindow;
import com.bank.Investment.InvestmentWindowPresenter;
import com.bank.Investment.InvestmentWindowPresenterFactory;
import com.bank.Login.LoginWindowPresenter;
import com.bank.Login.LoginWindowPresenterFactory;
import com.bank.Menu.Client;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.File;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Optional;


public class FxApplication extends Application {
    @Override
    public void start(Stage primaryStage) {
       // Pane root = FXMLLoader.load(getClass().getResource("sample.fxml"));

        Pane rootLayout = new VBox();

        Scene scene = new Scene(rootLayout);

        String url = Paths.get("src/main/java/com/bank/Style/bank_window_style.css").toUri().toString();
        scene.getStylesheets().add(url);

        LoginWindowPresenterFactory loginWindowPresenterFactory = new LoginWindowPresenterFactory(rootLayout);
        LoginWindowPresenter loginWindowPresenter = loginWindowPresenterFactory.create();
        loginWindowPresenter.showLogin();

        primaryStage.setResizable(false);
        primaryStage.setWidth(650);
        primaryStage.setHeight(500);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Bank");


        primaryStage.show();
    }

}

