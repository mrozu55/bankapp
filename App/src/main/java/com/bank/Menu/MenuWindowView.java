package com.bank.Menu;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;


public class MenuWindowView {
    private final Pane parent;
    private MenuWindowPresenter presenter;
    public MenuWindowView(Pane parent) {
        this.parent = parent;
    }

    public void setPresenter(MenuWindowPresenter presenter) {
        this.presenter = presenter;
    }

    public void showCreator(Client client){
        //Add later history of transaction
        Button show_button = new Button("Konto");
        Button transfer_button = new Button("Przelew");
        Button investment_button = new Button("Inwestuj");
        Button logout_button = new Button("Wyloguj");

        show_button.setOnMouseClicked(event -> { presenter.account();});
        transfer_button.setOnAction(event -> {presenter.transfer();});
        investment_button.setOnAction(event -> {presenter.goToInvestment();});
        logout_button.setOnAction(event -> {presenter.logout();});


        GridPane gridPane = new GridPane();
        gridPane.setMinSize(400, 200);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(5);
        gridPane.setHgap(5);
        gridPane.setAlignment(Pos.CENTER);
        gridPane.addRow(0, new Label(String.format("Witaj %s", client.getName())));
        gridPane.addRow(1, new Label("Menu Banku:"));
        gridPane.addRow(2, show_button);
        gridPane.addRow(3, transfer_button);
        gridPane.addRow(4, investment_button);
        gridPane.addRow(5, logout_button);
        parent.getChildren().clear();
        parent.getChildren().add(gridPane);
    }

    protected Pane getParent() {
        return parent;
    }
}
