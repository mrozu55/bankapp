package com.bank.Transfer;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

import javax.swing.text.MaskFormatter;
import javax.xml.validation.Validator;

public class TransferWindowView {
    private final Pane parent;
    private TransferWindowPresenter presenter;

    public TransferWindowView(Pane parent) {
        this.parent = parent;
    }

    public void setPresenter(TransferWindowPresenter presenter) {
        this.presenter = presenter;
    }


    public void showCreator() {

        TextField money = new TextField();
        TextField title = new TextField();
        TextField where = new TextField();



        Button transfer_button = new Button("Zatwierdz");
        transfer_button.setOnAction(event -> {presenter.transferMoney(money.getText(), title.getText(), where.getText());});
        Button cancel = new Button("Wróc");
        cancel.setOnAction(event -> {presenter.goBackToMenu();});

        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(5);
        gridPane.setHgap(5);
        gridPane.setAlignment(Pos.CENTER);

        gridPane.addRow(0, new Label("Przelew"));
        gridPane.addRow(1, new Label());
        gridPane.addRow(2, new Label("Nr konta: "), where);
        gridPane.addRow(3, new Label("Tytuł: "), title);
        gridPane.addRow(4, new Label("Ile: "), money);
        gridPane.addRow(5, transfer_button, cancel);


        parent.getChildren().clear();
        parent.getChildren().add(gridPane);
    }

    protected Pane getParent() {
        return parent;
    }
}
