package com.bank.Investment;

import com.bank.Investment.Stock.Stock;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Investment {

    private BigDecimal amount; //how much we payed
    private Stock stock;       //what stock we bought
    private double part;       //The part of the company we own

    public Investment(Stock stock) {
        this.stock = stock;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Stock getStock() {
        return stock;
    }

    public void buyStock(BigDecimal amount){
        this.amount = amount;
        part = stock.invest(amount);
    }

    public BigDecimal sellStock(){
        BigDecimal earned_money = stock.getAsset().multiply(BigDecimal.valueOf(part)).setScale(0, RoundingMode.UP);
        part = 0.0;
        System.out.println("Zysk = " + earned_money.subtract(amount));
        return earned_money;
    }
}
