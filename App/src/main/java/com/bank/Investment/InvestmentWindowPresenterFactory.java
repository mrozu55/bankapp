package com.bank.Investment;

import javafx.scene.layout.Pane;

public class InvestmentWindowPresenterFactory {
    final private Pane parent;

    public InvestmentWindowPresenterFactory(Pane parent) {
        this.parent = parent;
    }

    public InvestmentWindowPresenter create(){
        InvestmentWindowView view = new InvestmentWindowView(parent);
        InvestmentWindowPresenter presenter = new InvestmentWindowPresenter(view);
        view.setPresenter(presenter);
        return presenter;
    }
}
