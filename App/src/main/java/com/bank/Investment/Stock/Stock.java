package com.bank.Investment.Stock;

import javafx.application.Platform;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;
import java.util.Random;
import java.util.UUID;

public class Stock implements Runnable {
    private UUID uuid;
    private StockBean invest; //I use it to share the data about ratio and asset
    private double current_ratio;
    private BigDecimal asset;
    private BigDecimal liability;
    private double ratio_min;
    private double ratio_max;
    private String name;

    public Stock(){
        asset = new BigDecimal("10000");
        liability = new BigDecimal("0");
        current_ratio = 0.0;
        setDefaultRatioLvl();
        uuid = UUID.randomUUID();
    }

    public void setDefaultRatioLvl(){
        this.ratio_min = -1.5;
        this.ratio_max = 1.5;
    }

    public double invest(BigDecimal money){
        double part = money.divide(asset, 2, RoundingMode.UP).doubleValue();
        liability = liability.add(money);
        asset = asset.add(liability);
        return part;
    }

    private void simulateRatio(){
        //every few sec change ratio and asset
        Random random = new Random();
        current_ratio = random.nextFloat() * (ratio_max - ratio_min) + ratio_min;

    }

    @Override
    public void run() {
        Platform.runLater(() -> {
            //I used Platform.runLater to avoid error - Not on FX application thread; currentThread
            simulateRatio();
            updateAsset();
            invest.setRatio(current_ratio);
            invest.setAsset(asset);
        });
    }

    private void updateAsset() {
        double ratio = current_ratio / 100.0;
        double percent = ratio + 1.0;
        BigDecimal b_ratio = new BigDecimal(percent);
        asset = asset.multiply(b_ratio).setScale(0, RoundingMode.UP);
    }

    public BigDecimal getAsset() {
        return asset;
    }

    public void setStockBean(StockBean invest) {
        this.invest = invest;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Stock)) return false;
        Stock stock = (Stock) o;
        return Objects.equals(uuid, stock.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
