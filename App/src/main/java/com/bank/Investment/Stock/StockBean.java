package com.bank.Investment.Stock;


import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

import java.math.BigDecimal;


public class StockBean {
    DoubleProperty ratio;
    DoubleProperty asset;

    public StockBean(){
        ratio = new SimpleDoubleProperty();
        asset = new SimpleDoubleProperty();
    }

    public void setAsset(BigDecimal asset) {
        this.asset.set(asset.doubleValue());
    }

    public double getAsset() {
        return asset.get();
    }

    public double getRatio() {
        double result = ratio.get() * 100;
        result = Math.round(result);
        return result / 100;
    }

    public void setRatio(double ratio) {
        this.ratio.set(ratio);
    }

    public DoubleProperty okProperty() {
        return ratio;
    }
}
