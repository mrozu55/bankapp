package com.bank.Investment.Stock;

import com.bank.Investment.InvestmentWindowPresenter;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class StockView {
    private GridPane gridPane;
    final private InvestmentWindowPresenter presenter;
    private StockBean invest;
    private Stock stock;
    private Label ratio;
    private Label asset;
    Button buy_button;
    Button sell_button;

    public StockView(InvestmentWindowPresenter presenter, Stock stock) {
        invest = new StockBean();
        invest.okProperty().addListener((observable, oldValue, newValue) -> update());
        this.presenter = presenter;
        this.stock = stock;
        this.stock.setStockBean(invest);
        ratio = new Label(String.valueOf(invest.getRatio()));
        asset = new Label(String.valueOf(invest.getAsset()));
        buy_button = new Button("Kup");
        sell_button = new Button("Sprzedaj");
        gridPane = new GridPane();
    }

    private void update() {
        asset.setText(invest.getAsset() + "PLN");
        ratio.setText(invest.getRatio() + "%");
        if(invest.getRatio() > 0){
            ratio.setStyle("-fx-text-fill: green");
        }
        else if (invest.getRatio() == 0){
            ratio.setStyle("-fx-text-fill: black");
        }
        else{
            ratio.setStyle("-fx-text-fill: red");
        }
    }

    public void makeGrid(){
        asset.setMinWidth(80);
        asset.setAlignment(Pos.CENTER);
        ratio.setMinWidth(50);
        ratio.setAlignment(Pos.CENTER);


        buy_button.setOnAction(event -> {presenter.buyInvesting(this);});
        sell_button.setOnAction(event -> {presenter.sellInvesting(this);});
        sell_button.setDisable(true);


        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(5);
        gridPane.setHgap(5);
        gridPane.setAlignment(Pos.CENTER);
        gridPane.addRow(0, new Label(stock.getName()), asset, ratio, buy_button, sell_button);
    }


    public GridPane getGridPane() {
        return gridPane;
    }
    public Stock getStock(){
        return stock;
    }
    public void stockBought(){
        sell_button.setDisable(false);
        buy_button.setDisable(true);
        gridPane.setStyle("-fx-background-color: rgba(96, 213, 42, 0.53)");
    }

    public void stockSold(){
        sell_button.setDisable(true);
        buy_button.setDisable(false);
        gridPane.setStyle("-fx-background-color: white");
    }

}
