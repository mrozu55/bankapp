package com.bank.Investment;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

public class InvestmentWindowView {
    final private Pane parent;
    private GridPane gridPane;
    private InvestmentWindowPresenter presenter;

    public InvestmentWindowView(Pane parent) {
        this.parent = parent;
    }

    public void setPresenter(InvestmentWindowPresenter presenter) {
        this.presenter = presenter;
    }

    public void showCreator() {
        Label header = new Label("Inwestycje");

        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(5);
        gridPane.setHgap(5);
        gridPane.setAlignment(Pos.CENTER);
        gridPane.addRow(0, header);


        parent.getChildren().clear();
        parent.getChildren().add(gridPane);
    }

    public void addStockView(GridPane grid){
        parent.getChildren().add(grid);
    }
}
