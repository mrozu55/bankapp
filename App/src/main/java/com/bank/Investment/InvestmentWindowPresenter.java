package com.bank.Investment;

import com.bank.BankHelper;
import com.bank.DAO.ClientDAO_DB;
import com.bank.InformationWindow.BoxWindow;
import com.bank.Investment.Stock.*;
import com.bank.Menu.Client;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;


public class InvestmentWindowPresenter {

    private InvestmentWindowView view;
    private Client client;
    private List<Investment> investments;
    private char[] pass;
    public InvestmentWindowPresenter(InvestmentWindowView view) {
        this.view = view;
        investments = new ArrayList<>();
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void showCreator() {
        view.showCreator();
        for(int i=0; i<5; i++){
            addStockView();
        }

        Optional<char[]> authorization = BoxWindow.getAuthorization();
        authorization.ifPresent(chars -> pass = chars);
    }

    protected Stock runStock(){
        Stock stock = new Stock();
        stock.setName("Stock " + investments.size());
        ThreadFactory tf = r -> {
            Thread t = new Thread(r);
            t.setDaemon(true);
            return t;
        };
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor(tf);
        executor.scheduleAtFixedRate(stock, 0, 3, TimeUnit.SECONDS);
        return stock;
    }

    public void buyInvesting(StockView stockView) {
        Optional<Double> money_input = BoxWindow.inputMoney("Kupno inwestycji");
        if(money_input.isEmpty()) return;
        if(money_input.get() > client.getMoney().doubleValue()){
            BoxWindow.infoBox("Nie masz tyle piniędzy", "Błąd");
        }
        else {
            //Buy stock & transfer money
            Investment new_invest = new Investment(stockView.getStock());
            BigDecimal to_pay = BigDecimal.valueOf(money_input.get());
            new_invest.buyStock(to_pay);
            client = BankHelper.subtractMoney(client, to_pay);
            investments.add(new_invest);
            stockView.stockBought();
            new ClientDAO_DB().transferMoney(client, pass, "numer_konta_stock" , to_pay );
        }
    }

    public void addStockView() {
        StockView stockView = new StockView(this, runStock());
        stockView.makeGrid();
        view.addStockView(stockView.getGridPane());
    }

    public void sellInvesting(StockView stockView) {
        //New window
        Investment for_sale = null;
        for (Investment a: investments) {
            if(a.getStock() == stockView.getStock()){
                BigDecimal earned_money = a.sellStock();
                System.out.println("Zarobiono " + earned_money);
                client = BankHelper.addMoney(client, earned_money);
                //update in database as well
                for_sale = a;
            }
        }
        if(for_sale != null){
            investments.remove(for_sale);
            stockView.stockSold();
        }
    }
}
